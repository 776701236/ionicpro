import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AcceuilPage } from './acceuil.page';

const routes: Routes = [
  {
    path: 'menu/acceuil',
    component: AcceuilPage,
    children: [
        {
          path: 'vente',
          children: [
            {
              path: '',
              loadChildren: '../vente/vente.module#VentePageModule'
            }
          ]
                },
        {
          path: 'depense',
          children: [
            {
              path: '',
              loadChildren: '../depense/depense.module#DepensePageModule'
            }
          ]
        },
        {
          path: 'pret',
          children: [
            {
              path: '',
              loadChildren: '../pret/pret.module#PretPageModule'
            }
          ]
        },
        {
          path: '',
          redirectTo: 'menu/acceuil/vente',
          pathMatch: 'full'
        }
      ]
    },
    {
      path: '',
      redirectTo: 'menu/acceuil/vente',
      pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AcceuilPage]
})
export class AcceuilPageModule {}
