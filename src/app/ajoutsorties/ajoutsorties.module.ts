import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AjoutsortiesPage } from './ajoutsorties.page';

const routes: Routes = [
  {
    path: '',
    component: AjoutsortiesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AjoutsortiesPage]
})
export class AjoutsortiesPageModule {}
