import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficePage } from './benefice.page';

describe('BeneficePage', () => {
  let component: BeneficePage;
  let fixture: ComponentFixture<BeneficePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
