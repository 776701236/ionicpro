
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { AuthenticateService } from '../services/authentication.service'
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  validations_form: FormGroup;
  errorMessage: string = '';
  login: any;

  
  constructor( private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private http: HttpClient, private nativeHttp: HTTP,){

  }
  
  ngOnInit() {
 
    this.validations_form = this.formBuilder.group({
      Phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(9),
      ])),
      Password: new FormControl('', Validators.compose([
        Validators.minLength(4),
        Validators.required
      ])),
    });
  }

  validation_messages = {
    'Phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'Password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  homeUser(value){
    this.authService.login({
      Phone: value.Phone,
      Password:value.Password,
    })
    .then(res => {
      console.log(res);
      this.errorMessage = "";
      this.navCtrl.navigateForward('/menu/acceuil');
      console.log(value.Phone);
    }, err => {
      this.errorMessage = err.message;
      console.log(value.Password);
    });
 }
  goToRegisterPage(){
    this.navCtrl.navigateForward('/register');
  }
 
  public logo = {
    logo : "assets/images/fewnu_logooff.png",

  }

  public logo1 = {
    logo : "assets/images/Phone.png",

  }

  public logo2 = {
    logo : "assets/images/security.png",

  }


  

  
    
  
  

 /* async logForm(user){
    firebase.database().ref('fewnu_users').once('value').then(function(snapshot) {
      
      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
       
        if(childKey==user.phone){
          console.log(true);
        }else
        {
          this.router.navigateByUrl('/menu/acceuil');
        }


      })
    }); 
   }*/
      /*snapshot.val().f(element => {
      console.log(element.phone);
      console.log(element.password);
     });*/
        
    

  
      
    /*let res=this.authService.login(user.phone);
    if(res ==true){
      
      this.router.navigateByUrl('/menu/acceuil')
    }else{
      this.router.navigateByUrl('/menu/acceuil')
      const alert = await this.alertController.create({
        header: 'Erreur',
        message: 'Mot de passe ou telephone invalide.',
        buttons: ['ok'],
        
      });
      await alert.present();
  
    }
    }*/
  




    
  /*add(){
    this.afDB.list('aaaa').push({
       pseudo: 'sidy sogue'
    });
  }*/

  /*phone: string=""
  password: string=""*/



  

 /* phone = {}
  logForm() {
    console.log(this.phone)
  }*/
  
  

  

 /* constructor(public afAuth: AngularFireAuth) {}

  async login(){
    const { phone, password }= this
    try{

      const res = await this.afAuth.au(phone, passord)
    }catch(err){
      console.dir(err)
      if(err.code == "auth/user-not-found"){
        console.log("User not found")
      }
    }
  }*/

 /* aceuil(){
    this.router.navigateByUrl('/menu/acceuil');
  }*/

  

}
