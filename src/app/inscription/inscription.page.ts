import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticateService } from '../services/authentication.service';
import { NavController, AlertController } from '@ionic/angular';
//import * as firebase from 'firebase';




@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit{
  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
 
  validation_messages = {
   'nom_complet': [
      { type: 'required', message: 'Nom is required.' },
      { type: 'minlenght', message: 'Enter le Nom.' }
    ],
   'phone': [
     { type: 'required', message: 'Email is required.' },
     { type: 'minlenght', message: 'Enter indicatif.' }
     
   ],
   'password': [
     { type: 'required', message: 'Password is required.' },
     { type: 'minlength', message: 'Password must be at least 5 characters long.' }
   ],
   'cpassword': [
    { type: 'required', message: 'Password is required.' },
    { type: 'minlength', message: 'Password must be at least 5 characters long.' }
  ]
 };
 
  constructor(
    public  navCtrl: NavController,
    public  authService: AuthenticateService ,
    public  formBuilder: FormBuilder,private alertController: AlertController,
  ) {}
 
  ngOnInit(){
    this.validations_form = this.formBuilder.group({
      phone: new FormControl('', Validators.compose([
        Validators.required,
       // Validators.minLength(13),
        //Validators.pattern('^[a-zA-Z0-9_.+-]+$')
       Validators.minLength(9)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(4),
        Validators.required
      ])),
      nom_complet: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(1),
      ])),
      cpassword: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5),
      ])),
    });
  }
 
  async inscriptionUser(value){
    if(value.password===value.cpassword){
      this.authService.inscriptionUser({
        phone: value.phone,
        password: value.password,
        nom_complet: value.nom_complet,
      })
      .then((res) => {
       console.log(res);
       this.errorMessage = "";
       this.successMessage = "Votre compte a été créé avec succés.";
       console.log(value.phone)
     }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = "";
       console.log(value.nom_complet)
     });
    }else{
      const alert = await this.alertController.create({
        header : 'Erreur',
        message : "Les mots de passe ne sont pas les memes",
        buttons : ['ok'],
    });
    await alert.present();
  }
}
 
  goLoginPage(){
    this.navCtrl.navigateBack('');
  }
 
  
}

  /*inscriptionForm: FormGroup;
  inscrit = new Inscription();
  cpassword = '';
  authService: any;
  
    constructor(private alertController: AlertController, 
    private formBuilder: FormBuilder, 
    public navCtrl: NavController, public afDB: AngularFireDatabase){

  }
  ngOnInit() {
    this.inscriptionForm= this.formBuilder.group({
      'nom' : new FormControl(this.inscrit.nom , Validators.required) ,
      'phone' : new FormControl(this.inscrit.phone, Validators.required) ,
      'password' : new FormControl(this.inscrit.password, Validators.required) ,
      'cpassword' :new FormControl('', Validators.required),
  })
}*/

 // async insForm() {
         /*if(this.inscrit.password == this.cpassword){*/
        /* var ref = firebase.database().ref('test');
         var test = ref
         .child(this.inscrit.phone)
         .add(this.inscrit);
         alert('votre compte a été crée avec succe') ;
            
        
}*/
      
        
        /*else
         {
        
            const alert = await this.alertController.create({
              header : 'Erreur',
              message : "Les mots de passe ne sont pas les memes",
              buttons : ['ok'],
         }); 
          await alert.present();
         }
        }
*/

     

  

 

  


  /*nom: string= ""
  telephone: string= ""
  password: string = ""
  cpassword: string = ""

  constructor(public afAuth: AngularFireAuth) { }

  

  async inscription(){
    const {nom, telephone,password, cpassword} = this
    if(password !== cpassword){
      return console.error("Passwords don't match")

    }
    try{

      const res = await this.afAuth.auth.signInWithEmailAndPassword(telephone, password)
      console.log(res)
    }catch(error){
      console.dir(error)
      }
}*/

