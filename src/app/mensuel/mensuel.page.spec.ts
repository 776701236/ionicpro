import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensuelPage } from './mensuel.page';

describe('MensuelPage', () => {
  let component: MensuelPage;
  let fixture: ComponentFixture<MensuelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensuelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensuelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
