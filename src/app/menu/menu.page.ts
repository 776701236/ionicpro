import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  private selectedItem: any;
  

  public menus = [

    {title:"Journal", url:"menu/journal", icon:'send'},
    {title:"Hebdomadaire", url:"menu/hebdomadaire", icon:'send'},
    {title:"Mensual", url:"menu/mensual", icon:'send'},
    {title:"Inventaires", url:"menu/inventaires", icon:'send'},
    {title:"Marges Benefice", url:"menu/benefice", icon:'send'},
    
   ]

  constructor(private router:Router) { }

  ngOnInit() {
    
  }
  onMenuItem(m){
    this.router.navigateByUrl(m.url);

  }

  public menus1 = [

    {title1:"Gestion stock", url:"menu/gestionstckok", icon1:'send'},
    {title1:"Ajout Produit", url:"menu/ajoutproduit", icon1:'send'},
    
   ]


  onMenu1Item(m){
    this.router.navigateByUrl(m.url);

  }

  public menus2 = [

    {title2:"Nous Contactez", url:"menu/nouscontacter", icon2:'send'},
    {title2:"A Propos", url:"menu/aprospos", icon2:'send'},
    
   ]


  onMenu2Item(m){
    this.router.navigateByUrl(m.url);

  }

  public logo = {
    logo : "assets/images/money.png",

  }

}
