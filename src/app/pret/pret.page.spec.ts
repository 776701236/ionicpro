import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretPage } from './pret.page';

describe('PretPage', () => {
  let component: PretPage;
  let fixture: ComponentFixture<PretPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
