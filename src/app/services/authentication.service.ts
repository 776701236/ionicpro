import { Injectable } from '@angular/core';
//import * as firebase from 'firebase/app';
import {apiKey} from "../../app/apiurls/serverurls.js";
import {Http, Headers} from '@angular/http';
import { Storage } from '@ionic/storage';


@Injectable()
export class AuthenticateService {

  

  public token: any;
 

  constructor(public storage: Storage, public http: Http) {

   }

   inscriptionUser(value){    
    return new Promise<any>((resolve, reject) => {
     let headers = new Headers();
      headers.append('Content-Type', 'Application/json');

      this.http.post(apiKey+'api/registeruser/', JSON.stringify(value), {headers:headers})
        .subscribe(res =>{
         let data = res.json;
         resolve(data);
      
      }, err => {
        reject(err)
      });
    });
   }

   login(value){    
    return new Promise<any>((resolve, reject) => {
     let headers = new Headers();
      headers.append('Access-Control-Allow-Origine', 'http://192.168.1.69:8000/');
      headers.append('Accept', 'application/json');
      headers.append('Content-Type', 'application/json');
      headers.append('Access-Control-Allow-Credentials', 'true');
      headers.append('GET', 'POST');
  

      this.http.post(apiKey+'api/loginuser/', JSON.stringify(value), {headers:headers})
        .subscribe(res =>{
         let data = res.json;
         resolve(data);
         this.token = data['token'];
         this.storage.set('token', this.token);
      
      }, err => {
        reject(err)
      });
    });
   }





 
  /*inscriptionUser(value){    
    return new Promise<any>((resolve, reject) => {
     firebase.database().ref('test').child(value.phone).set({
      phone: value.phone,
      password: value.password,
      name:value.name
      })
      .then(
        res => resolve(res),
        err => reject(err))
    })
   }
  
   homeUser(value){
    return new Promise<any>((resolve, reject) => {
      
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      .then(
        res => resolve(res),
        err => reject(err)),
        console.log(value)
    })
    
   }
  
   logoutUser(){
     return new Promise((resolve, reject) => {
       if(firebase.auth().currentUser){
         firebase.auth().signOut()
         .then(() => {
           console.log("LOG Out");
           resolve();
         }).catch((error) => {
           reject();
         });
       }
     })
   }
  
   userDetails(){
     return firebase.auth().currentUser;
   }*/
 }



  /*ngOnInit() {
          /*firebase.database().ref('fewnu_users').once('value').then(function(snapshot) {
            
            snapshot.forEach(function(childSnapshot) {
              var childKey = childSnapshot.key;
              var childData = childSnapshot.val();
            
              console.log(childData.password);
              console.log(childData.phone);


            })
          });*/

  
     /* login(user){

        firebase.database().ref('fewnu_users').once('value').then(function(snapshot) {
      
          snapshot.forEach(function(childSnapshot) {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();
            
            if(childKey===user.phone){
              this.authenticated=true;
            }else{
              this.authenticated=false;
            }          
          })
          
        });
        return this.authenticated
      } 
      }*/


