import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentePage } from './vente.page';

describe('VentePage', () => {
  let component: VentePage;
  let fixture: ComponentFixture<VentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
